import os
from os.path import isfile, join, exists
from os import listdir, mkdir
import command
import submodule
import re
import menu

plugins_path = join(os.getcwd(), 'Plugins')
setup_path = join(os.getcwd(), 'setup')
unrealplugins_path = join(setup_path, 'unrealplugins')
pluginlist_path = join(unrealplugins_path, 'plugins.config')
defaultplugins_path = join(unrealplugins_path, 'default_plugins.config')

def get_available_plugin_ids():
    if not exists(plugins_path):
        mkdir(plugins_path)

    if not exists(pluginlist_path):
        command.run(['git', 'submodule', 'update', '--init', '--', 'unrealplugins'], cwd=setup_path)
    command.run(['git', 'checkout', 'master'], unrealplugins_path)
    command.run(['git', 'pull'], unrealplugins_path)

    _, stdout, _ = command.run(['git', 'config', '-f', pluginlist_path, '--name-only', '--get-regexp', '.*.name'])

    plugin_ids = [name[:-5] for name in stdout.split('\n')]
    plugin_ids.remove('')
    return plugin_ids


def get_submodule_id(plugin_id):
    return 'Plugins/{}'.format(plugin_id)


def get_default_plugins(version):
    if not exists(defaultplugins_path):
        command.run(['git', 'submodule', 'update', '--init', '--', 'unrealplugins'], cwd=setup_path)
    command.run(['git', 'checkout', 'master'], unrealplugins_path)
    command.run(['git', 'pull'], unrealplugins_path)
    _, stdout, _ = command.run(['git', 'config', '-f', defaultplugins_path, '--name-only', '--get-regexp', 'version\\.{}.*'.format(version)])

    plugin_ids = [name[len('version.{}'.format(version)) + 1:] for name in stdout.split('\n')]
    plugin_ids.remove('')
    return plugin_ids


def get_default_branch(version, plugin_id):
    exit_code, stdout, _ = command.run(['git', 'config', '-f', defaultplugins_path, '--get', 'version.{}.{}'.format(version, plugin_id)], show_message_on_error=False)

    if exit_code == 0:
        return stdout[:-1]
    else:
        return None


def get_property(plugin_id, property_name):
    exit_code, stdout, _ = command.run(['git', 'config', '-f', pluginlist_path, '--get', '{}.{}'.format(plugin_id, property_name)], show_message_on_error=False)
    if exit_code == 0:
        return stdout[:-1]
    else:
        return None


def get_branch_property(plugin_id, branch, property_name):
    return get_property(plugin_id, '{}.{}'.format(branch, property_name))


def get_name(plugin_id):
    return get_property(plugin_id, 'name')


def get_url(plugin_id):
    return get_property(plugin_id, 'url')


def is_installed(plugin_id):
    submodule_id = get_submodule_id(plugin_id)
    return submodule.is_installed(submodule_id)


def get_installed_branch(plugin_id):
    submodule_id = get_submodule_id(plugin_id)
    if submodule.is_installed(submodule_id):
        return submodule.get_branch(submodule_id)
    else:
        return None


def get_short_commit_hash(plugin_id):
    submodule_id = get_submodule_id(plugin_id)
    submodule_path = submodule.get_path(submodule_id)
    _, hash, _ = command.run(['git', 'rev-parse', '--short', 'HEAD'], cwd=submodule_path)
    return hash[:-1]


def get_supported_unreal_versions(plugin_id, branch):
    unreal_versions_string = get_branch_property(plugin_id, branch, 'unreal-versions')
    if unreal_versions_string == None:
        return None
    else:
        return unreal_versions_string.split(',')


def get_available_branches(plugin_id):
    url = get_property(plugin_id, 'url')
    branches = command.run(['git', 'ls-remote', '--heads', url])[1][:-1].split('\n')
    branches = [re.match('[0-9a-fA-F]+\\s+refs/heads/(.+)$', b).groups()[0] for b in branches]
    return branches


def checkout_to_branch(plugin_id, new_branch):
    if is_installed(plugin_id):
        submodule_id = get_submodule_id(plugin_id)
        submodule_path = submodule.get_path(submodule_id)
        command.run(['git', 'checkout', new_branch], cwd=submodule_path)
        submodule.set_branch(submodule_id, new_branch)
    else:
        url = get_url(plugin_id)
        exit_status, _, error = command.run(['git', 'submodule', 'add', '--quiet', '-b', new_branch, url, './Plugins/{}'.format(plugin_id)], show_message_on_error=False)
        if exit_status != 0:
            if menu.show_yes_no('Git Error:\n\n' + error + '\nTry again with \'--force\'?', default_selection=False):
                command.run(['git', 'submodule', 'add', '--quiet', '--force', '-b', new_branch, url, './Plugins/{}'.format(plugin_id)])
            else:
                return


def uninstall(plugin_id):
    if not is_installed(plugin_id):
        return

    submodule_id = get_submodule_id(plugin_id)
    submodule_path = submodule.get_path(submodule_id)
    exit_status, _, error = command.run(['git', 'submodule', 'deinit', submodule_path], show_message_on_error=False)
    forced = False
    if exit_status != 0:
        if menu.show_yes_no('Git Error:\n\n' + error + '\nTry again with \'--force\'?', default_selection=False):
            command.run(['git', 'submodule', 'deinit', '--force', submodule_path])
            forced = True
        else:
            return
    
    if forced:
        exit_status, _, error = command.run(['git', 'rm', '--force', submodule_path])
    else:
        exit_status, _, error = command.run(['git', 'rm', submodule_path])
    