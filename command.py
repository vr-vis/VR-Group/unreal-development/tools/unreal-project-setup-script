import subprocess
import os
import os.path
import menu
import datetime

def open_logfile():
    logs_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs')
    if not os.path.exists(logs_directory):
        os.mkdir(logs_directory)
    now = datetime.datetime.now()
    logfile_name = now.strftime("%Y-%m-%d_%A_%H-%M-%S.log")
    return open(os.path.join(logs_directory, logfile_name), 'w')


logfile = open_logfile()

def run(command, cwd=None, show_message_on_error=True):
    cwd = cwd if cwd != None else os.getcwd()

    process = subprocess.Popen(command, encoding='utf-8', cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return_value = process.wait()
    stdout, stderr = process.communicate()

    logfile.write('{} > {} [{}]\n'.format(cwd, ' '.join(command), return_value))
    if len(stdout) > 0:
        logfile.write('STDOUT:\n')
        logfile.write(stdout)
    if len(stderr) > 0:
        logfile.write('STDERR:\n')
        logfile.write(stderr)
    logfile.write('\n')

    if return_value != 0 and show_message_on_error:
        menu.show_message('Command failed: ' + ' '.join(command) + '\n\n Error message:\n' + stderr)
    
    return return_value, stdout, stderr