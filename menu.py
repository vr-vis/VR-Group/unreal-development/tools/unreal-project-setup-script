import sys
import os

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen. From http://code.activestate.com/recipes/134892/"""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            try:
                self.impl = _GetchMacCarbon()
            except(AttributeError, ImportError):
                self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys, termios # import termios now or else you'll get the Unix version on the Mac

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getwch()

class _GetchMacCarbon:
    """
    A function which returns the current ASCII key that is down;
    if no ASCII key is down, the null string is returned.  The
    page http://www.mactech.com/macintosh-c/chap02-1.html was
    very helpful in figuring out how to do this.
    """
    def __init__(self):
        import Carbon
        Carbon.Evt #see if it has this (in Unix, it doesn't)

    def __call__(self):
        import Carbon
        if Carbon.Evt.EventAvail(0x0008)[0]==0: # 0x0008 is the keyDownMask
            return ''
        else:
            #
            # The event contains the following info:
            # (what,msg,when,where,mod)=Carbon.Evt.GetNextEvent(0x0008)[1]
            #
            # The message (msg) contains the ASCII char which is
            # extracted with the 0x000000FF charCodeMask; this
            # number is converted to an ASCII character with chr() and
            # returned
            #
            (what,msg,when,where,mod)=Carbon.Evt.GetNextEvent(0x0008)[1]
            return chr(msg & 0x000000FF)

def get_key():
    inkey = _Getch()

    value = ''
    while True:
        k = inkey()
        value = value + k
        # 1st check if Esc was hit twice: then only return 1x Esc
        if (value == '\x1b\x1b'):return k
        # Else read until no special characters are found anymore
        elif k == '\x00' or k == '\xe0' or k == '\x1b' or k == '[': pass
        elif k != '':break
    return value

# Different platforms have different escape keys
arrow_up = ['\x00H', '\xe0H', '\x1b[A']
arrow_down = ['\x00P', '\xe0P', '\x1b[B']
arrow_right = ['\x00M', '\xe0M', '\x1b[C']
arrow_left = ['\x00K', '\xe0K', '\x1b[D']
enter = '\x0d'
escape = ['\x1b', 'q', 'Q']

class Terminal:
    REVERSE = '\u001b[7m'
    RESET = '\u001b[0m'
    BOLD = '\u001b[1m'
    CLEAR_SCREEN = '\u001b[2J'
    RESET_CURSOR = '\u001b[0;0H'

    FG_RED = '\u001b[31m'
    FG_GREEN = '\u001b[32m'
    FG_YELLOW = '\u001b[33m'

    @staticmethod
    def clear_screen():
        sys.stdout.write(Terminal.CLEAR_SCREEN)

    @staticmethod
    def set_cursor_position(column, row):
        sys.stdout.write('\u001b[{};{}H'.format(row, column))

    @staticmethod
    def flush():
        sys.stdout.flush()

    @staticmethod
    def write(text):
        sys.stdout.write(text)

    @staticmethod
    def write_formatted(format_codes, text, clear=''):
        sys.stdout.write(format_codes)
        sys.stdout.write(text)
        sys.stdout.write(clear)

DEFAULT_BOTTOM_TEXT = '[Arrow Up/Down] Change Selection\n' + \
                      '[Enter]         Confirm Selection\n' + \
                      '[Esc]/[Q]       Abort'

def showmenu(menu_items, top_text=None, bottom_text=DEFAULT_BOTTOM_TEXT, default_selection=0):
    """Displays a menu.

    Keyword arguments:
    menu_items -- 
    """
    os.system('') # Magic
    current_selection = default_selection
    while True:
        Terminal.clear_screen()
        Terminal.set_cursor_position(0,0)

        if top_text != None:
            Terminal.write(top_text)
            Terminal.write('\n\n')

        for i in range(len(menu_items)):
            if i == current_selection:
                Terminal.write('> ')
                Terminal.write(Terminal.REVERSE + menu_items[i])
                Terminal.write('\n')
            else:
                Terminal.write('  {}\n'.format(menu_items[i]))
            Terminal.write(Terminal.RESET)
        Terminal.flush()

        if bottom_text != None:
            Terminal.write('\n\n')
            Terminal.write(bottom_text)

        k = get_key()
        if k in arrow_up:
            current_selection = max(0, current_selection - 1)
        elif k in arrow_down:
            current_selection = min(len(menu_items) - 1, current_selection + 1)
        elif k == enter:
            Terminal.clear_screen()
            return current_selection
        elif k in escape:
            Terminal.clear_screen()
            return None

def show_yes_no(text, default_selection=True):
    return showmenu([Terminal.FG_GREEN + 'Yes', Terminal.FG_RED + 'No'], top_text=text, default_selection=0 if default_selection else 1) == 0

def show_message(message):
    return showmenu(['OK'], message, bottom_text='Press Enter to continue')
