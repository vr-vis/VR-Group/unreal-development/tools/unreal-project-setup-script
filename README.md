# Setup Script

The setup script to initialize new unreal engine projects based on our [template](https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/unrealprojecttemplate) as well as managing their plugins.