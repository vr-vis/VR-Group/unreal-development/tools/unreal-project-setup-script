#!/usr/bin/env python

from os import listdir
import menu
import plugin
import json
import command
import sys

unreal_version=''

def get_project_file():
    for file in listdir('.'):
        if file.endswith('.uproject'):
            return file
    return None

def initial_setup():
    if not menu.show_yes_no('The setup will now delete all branches and remotes and create a default develop and master branch. Continue?', default_selection=False):
        return
    command.run(['git', 'lfs', 'install'])
    command.run(['git', 'lfs', 'track', '*.uasset'])
    current_branch = command.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'])[1][:-1]
    branches_to_delete = command.run(['git', 'for-each-ref', '--format=%(refname:short)', 'refs/heads/'])[1].split('\n')
    branches_to_delete.remove('')

    # Add changes from unreal
    command.run(['git', 'add', '.'])
    command.run(['git', 'commit', '-m', "Setup project"])

    for branch in branches_to_delete:
        if branch != current_branch:
            command.run(['git', 'branch', '-d', branch])
    if current_branch != "develop":
        command.run(['git', 'checkout', '-b', 'develop'])
        command.run(['git', 'branch', '-d', current_branch])

    setup_default_plugins()
    menu.show_message("You are now ready to select the plugins to install")
    manage_plugins()

    command.run(['git', 'branch', 'master'])

    remote = command.run(['git', 'remote'])[1][:-1]
    if len(remote) > 0:
        command.run(['git', 'remote', 'remove', remote])

    if menu.show_yes_no('Do you want to add a new origin url?'):
        origin_url = input('Enter URL of the project\'s git repository (Leave empty to skip this step):')
        if len(origin_url) > 0:
            command.run(['git', 'remote', 'add', 'origin', origin_url])
            command.run(['git', 'push', '-u', 'origin', 'develop'])
            command.run(['git', 'checkout', 'master'])
            command.run(['git', 'push', '-u', 'origin', 'master'])
            command.run(['git', 'checkout', 'develop'])

def get_branch_color(plugin_id, branch):
    supported_versions = plugin.get_supported_unreal_versions(plugin_id, branch)
    if supported_versions == None:
        return menu.Terminal.FG_YELLOW
    elif unreal_version in supported_versions:
        return menu.Terminal.FG_GREEN
    else:
        return menu.Terminal.FG_RED

def manage_plugin(plugin_id):
    available_branches = plugin.get_available_branches(plugin_id)
    branch_text = [get_branch_color(plugin_id, branch) + branch for branch in available_branches]
    selected_item = menu.showmenu(branch_text + ['<remove>'])

    if selected_item == None:
        pass
    elif selected_item < len(available_branches):
        plugin.checkout_to_branch(plugin_id, available_branches[selected_item])
    else:
        plugin.uninstall(plugin_id)

def get_plugin_string(plugin_id):
    plugin_string = ''

    if plugin.is_installed(plugin_id):
        plugin_string += menu.Terminal.BOLD
    
    plugin_string += plugin.get_name(plugin_id)

    if plugin.is_installed(plugin_id):
        plugin_string += ': '
        branch = plugin.get_installed_branch(plugin_id)
        if branch != None:
            plugin_string += get_branch_color(plugin_id, branch)
            plugin_string += branch
        else:
            plugin_string += plugin.get_short_commit_hash(plugin_id)

    return plugin_string

def manage_plugins():
    while True:
        plugin_ids = plugin.get_available_plugin_ids()
        menu_items = [get_plugin_string(plugin_id) for plugin_id in plugin_ids]
        BOTTOM_TEXT = '[Arrow Up/Down] Select Plugin\n' + \
                      '[Enter]         Select Plugin branch\n' + \
                      '[Esc]/[Q]       Confirm selection'
        plugin_selection = menu.showmenu(menu_items, bottom_text = BOTTOM_TEXT)

        if plugin_selection == None:
            break
        else:
            manage_plugin(plugin_ids[plugin_selection])

    if menu.show_yes_no("Commit changes?"):
        command.run(['git', 'commit', '-m', "Update plugins"])

def setup_default_plugins():
    for plugin_id in plugin.get_default_plugins(unreal_version):
        branch = plugin.get_default_branch(unreal_version, plugin_id)
        plugin.checkout_to_branch(plugin_id, branch)


def main():
    uproject_file = get_project_file()
    if not uproject_file:
        print('This script should be run from the root directory of the unreal project.')
        exit(-1)

    uproject = json.load(open(uproject_file))
    global unreal_version
    unreal_version = uproject['EngineAssociation']

    if len(sys.argv) > 1 and sys.argv[1] == '--install-default-plugins':
        print('Installing default plugins...')
        setup_default_plugins()
    else:
        while True:
            selection = menu.showmenu(['Initial Setup', 'Manage Plugins', 'Quit'])
            if selection == 0:
                initial_setup()
            elif selection == 1:
                manage_plugins()
            elif selection == 2 or selection == None:
                break

if __name__ == "__main__":
    main()