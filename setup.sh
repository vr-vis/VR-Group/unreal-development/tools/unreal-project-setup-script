#!/bin/sh

function initial_setup() {
    clear

    echo "The setup will now delete all branches and remotes and create a default develop and master branch. Continue? [Y/n]"
    read cont
    if [ "$cont" == "y" ] || [ "$cont" == "Y" ] || [ "$cont" == "" ]
    then
        git lfs install
        git lfs track '*.uasset'

        current_branch=$(git rev-parse --abbrev-ref HEAD)
        branches_to_delete=$(git for-each-ref --format='%(refname:short)' refs/heads/)

        for branch in $branches_to_delete
        do
            if [ "$branch" != "$current_branch" ]
            then
                git branch -d "$branch"
            fi
        done

        if [ "$current_branch" != "develop" ]
        then
            git checkout -b develop
            git branch -d "$current_branch"
        fi

        echo "Press [enter] to select plugins."
        read x
        manage_plugins "initial_setup"
        
        git add .
        git commit -m "Initial commit"
        git branch master

        git remote | xargs -n1 git remote remove
        echo "Enter URL of the project's git repository (Leave empty to skip this step):"
        read new_remote

        if [ -n "$new_remote" ]
        then
            echo "Setting up repository..."
            git remote add origin $new_remote
            git push -u origin develop
            git checkout master
            git push -u origin master
            git checkout develop
        fi
        echo "Done!"
    else
        echo "Skipping setup."
    fi
}

function manage_plugins() {
    clear
    echo "Loading plugin list..."
    mkdir -p Plugins
    path=$(git config -f .gitmodules --get "submodule.Plugins/unrealplugins.path")
    if [ $? -ne 0 ]
    then
        git submodule add -f -b master https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/unrealplugins.git Plugins/unrealplugins
    else
        cd Plugins/unrealplugins
        git pull
        cd ../..
    fi

    SAVEIFS=$IFS
    IFS=$'\n'
    preset_list=($(git config -f Plugins/unrealplugins/presets.config --name-only --get-regexp ".*.name"))
    plugin_list=($(git config -f Plugins/unrealplugins/plugins.config --name-only --get-regexp ".*.name"))
    IFS=$SAVEIFS

    declare -a preset_ids
    declare -a preset_descriptions
    declare -a presets

    for i in ${!preset_list[@]}
    do
        preset_id=${preset_list[$i]%.name}
        preset_ids+=("$preset_id")
        preset_name=$(git config -f Plugins/unrealplugins/presets.config --get "$preset_id.name")
        presets+=("${preset_name}")
        preset_description=$(git config -f Plugins/unrealplugins/presets.config --get "$preset_id.description")
        preset_descriptions+=("${preset_description}")
    done

    declare -a plugin_ids
    declare -a plugins
    declare -a plugin_repositories
    declare -a plugin_branches

    for i in ${!plugin_list[@]}
    do
        plugin_id=${plugin_list[$i]%.name}
        plugin_ids+=("$plugin_id")
        plugin_name=$(git config -f Plugins/unrealplugins/plugins.config --get "$plugin_id.name")
        plugins+=("${plugin_name}")
        plugin_repositories+=($(git config -f Plugins/unrealplugins/plugins.config --get "$plugin_id.url"))

        if [ "$1" == "initial_setup" ]
        then
            plugin_branches+=("")
        else
            path=$(git config -f .gitmodules --get "submodule.Plugins/${plugin_id}.path")
            if [ $? == 0 ]
            then
                branch=$(git config -f .gitmodules --get "submodule.Plugins/${plugin_id}.branch")
                if [ $? -ne 0 ]
                then
                    plugin_branches+=("master")
                else
                    plugin_branches+=("$branch")
                fi
            else
                plugin_branches+=("")
            fi
        fi
    done

    if [ "$1" == "initial_setup" ]
    then
        selection=0
        exit=false
        cancel=false
        num_presets_minus_one=${#presets[@]}
        let num_presets_minus_one-=1

        while [ $exit == "false" ]
        do
            clear

            echo "Select plugin preset:"
            echo ""

            for i in ${!presets[@]}
            do
                if [ $i == $selection ]
                then
                    printf "\e[7m"
                fi

                echo " ${presets[$i]}"

                if [ $i == $selection ]
                then
                    printf "\e[0m"
                fi
            done

            echo ""
            echo "${preset[$selection]}"
            echo "${preset_descriptions[$selection]}"
            
            echo ""
            echo "[Arrow Up/Down] Select preset"
            echo "[Enter]         Continue"
            echo "[Esc]           Skip preset selection"

            SAVEIFS=$IFS
            IFS=''
            escape_char=$(printf "\033")
            escape=false
            read -rsn1 mode # get 1 character
            if [[ $mode == $escape_char ]]; then
                escape=true
                read -rsn2 -t 0.1 mode # read 2 more chars
            fi
            IFS=$SAVEIFS
            case $mode in
                "") if [ "$escape" == true ]; then cancel=true; fi; exit=true ;;
                "[D") previous_branch $selection ;;
                "[C") next_branch $selection ;;
                "[A") if [ "$selection" -gt "0" ]; then let selection-=1; fi ;;
                "[B") if [ "$selection" -lt "$num_presets_minus_one" ]; then let selection+=1; fi ;;
                *) >&2 ;;
            esac
            
        done

        # Set the appropriate plugin branches
        if [ "$cancel" != true ]
        then
            SAVEIFS=$IFS
            IFS=$'\n'
            preset_plugins=($(git config -f Plugins/unrealplugins/presets.config --name-only --get-regexp "${preset_ids[$selection]}\.plugins\..*"))
            IFS=$SAVEIFS
            
            for plugin in ${preset_plugins[@]}
            do
                plugin_id=${plugin#"${preset_ids[$selection]}.plugins."}
                for i in ${!plugins[@]}
                do
                    if [ $plugin_id == ${plugin_ids[$i]} ]
                    then
                        branch=$(git config -f Plugins/unrealplugins/presets.config --get "$plugin")
                        plugin_branches[$i]=$branch
                    fi
                done
            done
        fi
    fi

    function next_branch() {
        case ${plugin_branches[$1]} in
            "") plugin_branches[$1]="master" ;;
            "master") plugin_branches[$1]="develop" ;;
            "develop") plugin_branches[$1]="" ;;
            *) >&2; plugin_branches[$1]="" ;;
        esac
    }

    function previous_branch() {
        case ${plugin_branches[$1]} in
            "master") plugin_branches[$1]="" ;;
            "develop") plugin_branches[$1]="master" ;;
            "") plugin_branches[$1]="develop" ;;
            *) >&2; plugin_branches[$1]="" ;;
        esac
    }

    selection=0
    exit=false
    cancel=false
    num_plugins_minus_one=${#plugins[@]}
    let num_plugins_minus_one-=1

    while [ $exit == "false" ]
    do
        clear

        echo "Select plugins to install:"
        echo ""

        for i in ${!plugins[@]}
        do
            if [ $i == $selection ]
            then
                printf "\e[7m"
            fi

            if [ -z ${plugin_branches[$i]} ]
            then
                echo " [ ] ${plugins[$i]}"
            else
                echo " [X] ${plugins[$i]}: ${plugin_branches[$i]}"
            fi

            if [ $i == $selection ]
            then
                printf "\e[0m"
            fi
        done

        
        echo ""
        echo "[Arrow Up/Down]    Select Plugin"
        echo "[Arrow Left/Right] Select branch"
        echo "[Enter]            Continue"
        echo "[Esc]              Cancel"

        SAVEIFS=$IFS
        IFS=''
        escape_char=$(printf "\033")
        escape=false
        read -rsn1 mode # get 1 character
        if [[ $mode == $escape_char ]]; then
            escape=true
            read -rsn2 -t 0.1 mode # read 2 more chars
        fi
        IFS=$SAVEIFS
        case $mode in
            "") if [ "$escape" == true ]; then cancel=true; fi; exit=true ;;
            "[D") previous_branch $selection ;;
            "[C") next_branch $selection ;;
            "[A") if [ "$selection" -gt "0" ]; then let selection-=1; fi ;;
            "[B") if [ "$selection" -lt "$num_plugins_minus_one" ]; then let selection+=1; fi ;;
            *) >&2 ;;
        esac
        
    done

    if [ "$cancel" != true ]
    then
        clear
        echo "Adding/updating plugins..."

        for i in ${!plugins[@]}
        do
            # Check if plugin is already present
            path=$(git config -f .gitmodules --get "submodule.Plugins/${plugin_ids[$i]}.path")
            if [ $? -ne 0 ]
            then
                plugin_present=false
            else
                plugin_present=true
            fi

            if [ -n "${plugin_branches[$i]}" ]
            then
                if [ "$plugin_present" == false ]
                then
                    # Plugin was not yet added, call git submodule add
                    echo "Add new plugin: ${plugins[$i]}"
                    cd Plugins
                    git submodule add -f -b ${plugin_branches[$i]} ${plugin_repositories[$i]}
                    cd ..
                else
                    # Updating existing plugin
                    echo "Update plugin: ${plugins[$i]}"
                    cd "Plugins/${plugin_ids[$i]}"
                    git fetch
                    git checkout ${plugin_branches[$i]}
                    git pull
                    cd ../..
                fi
            else
                if [ "$plugin_present" == true ]
                then
                    # Remove plugin!
                    git rm -f "Plugins/${plugin_ids[$i]}"
                fi
            fi
        done
        echo "Done, Press enter to continue."
        read x
    fi
}

function show_main_menu() {
    selection=0
    exit=false
    quit=false

    while [ "$exit" == false ]
    do
        clear

        echo "Select Task:"
        echo ""

        items=("InitialSetup" "ManagePlugins" "Exit")

        for i in ${!items[@]}
        do
            if [ $i == $selection ]
            then
                printf "\e[7m* "
            else
                printf "  "
            fi

            echo "${items[$i]}"

            if [ $i == $selection ]
            then
                printf "\e[0m"
            fi
        done

        echo ""
        echo "[Arrow Up/Down] Change Selection"
        echo "[Enter]         Select"
        echo "[Esc]           Quit"

        SAVEIFS=$IFS
        IFS=''
        escape_char=$(printf "\033")
        escape=false
        read -rsn1 mode # get 1 character
        if [[ $mode == $escape_char ]]; then
            escape=true
            read -rsn2 -t 0.1 mode # read 2 more chars
        fi
        IFS=$SAVEIFS 
        case $mode in
            "") if [ "$escape" = true ]; then quit=true; fi; exit=true ;;
            "[A") if [ "$selection" -gt "0" ]; then let selection-=1; fi ;;
            "[B") if [ "$selection" -lt "2" ]; then let selection+=1; fi ;;
            *) >&2 ;;
        esac
    done

    if [ "$escape" == false ]
    then
        case $selection in
            0) initial_setup; show_main_menu ;;
            1) manage_plugins; show_main_menu ;;
            2) ;;
            *) >&2;;
        esac
    fi
}

show_main_menu
clear