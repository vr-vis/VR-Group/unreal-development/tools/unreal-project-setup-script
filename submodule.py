import command

# def get_submodule_id_for_repository(repository_url):
#     pass

def is_installed(submodule_id):
    return get_property(submodule_id, 'path') != None


def get_property(submodule_id, property_name):
    exit_code, stdout, _ = command.run(['git', 'config', '-f', '.gitmodules', '--get', 'submodule.{}.{}'.format(submodule_id, property_name)], show_message_on_error=False)
    if exit_code == 0:
        return stdout[:-1]
    else:
        return None


def set_property(submodule_id, property_name, property_value):
    exit_code, stdout, _ = command.run(['git', 'config', '-f', '.gitmodules', '--unset-all', 'submodule.{}.{}'.format(submodule_id, property_name)])
    exit_code, stdout, _ = command.run(['git', 'config', '-f', '.gitmodules', '--add', 'submodule.{}.{}'.format(submodule_id, property_name), property_value])
    if exit_code == 0:
        return stdout[:-1]
    else:
        return None


def get_branch(submodule_id):
    return get_property(submodule_id, 'branch')


def set_branch(submodule_id, branch):
    return set_property(submodule_id, 'branch', branch)


def get_path(submodule_id):
    return get_property(submodule_id, 'path')